import { combineReducers } from 'redux'

import { layers } from './layers'
import { dialog } from './dialog'
import { scores } from './scores'
import { apikey } from './apikey'
import { params } from './params'
import { loading } from './loading'

const mapApp = combineReducers({
    layers,
    scores,
    dialog,
    apikey,
    params,
    loading
})

export default mapApp