export const params = (state = {}, action) => {
    switch (action.type) {
        case 'SET_PARAMS':
            return Object.assign({}, state, {
              ...action.data
            })
        default:
            return state
    }
}
