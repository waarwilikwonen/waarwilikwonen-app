import statistics from 'simple-statistics'
import turf from 'turf'


const getBreaks = (data, field) => {
  const values = data.map((row) => row[field])

  let breaks = statistics.ckmeans(values, 10)
  breaks.splice(breaks.indexOf(null), 1)
  console.log(field, breaks)
  return breaks
}

const getBreakPoint = (value, breaks) => {
  if (value === null) {
    return 0
  }
  for (var i = 0; i < breaks.length; i++) {
    // if (value >= breaks[i] && value < breaks[i + 1])
    if (breaks[i].indexOf(value) !== -1)
      break
  }
  return i
}

const mapRegionsWithinDistance = (regions, data, field, max_distance, filter_null = true) => {
  let newRegions = regions.map((region) => {
    const n = data.filter((row) => {
      let feature = row.geom
      if (feature.type !== 'Point') {
        feature = turf.centroid({
          type: 'Feature',
          geometry: feature
        })
      }
      const distance = turf.distance(region.geom, feature, 'meters')
      return distance <= max_distance
    }).length
    if (region.postcode === '4351') {
      console.log(region.postcode, field, n, max_distance)
    }
    region[field] = n
    return region
  })

  if (filter_null) {
    newRegions = newRegions.filter((row) => {
      return row[field] > 0
    })
  }

  return newRegions
}

const populateHousing = (centroids, data) => {
  if (!data) {
    return []
  }

  const breaks = getBreaks(data, 'price')
  return data.map((row) => {
    row.score = 10 - getBreakPoint(row.price, breaks)
    return row
  })
}

const populateWork = (regions, data) => {
  if (!data) {
    return []
  }
  const work = mapRegionsWithinDistance(regions, data, 'njobs', 50000)
  const breaks = getBreaks(work, 'njobs')
  return work.map((row) => {
    row.score = getBreakPoint(row.njobs, breaks)
    return row
  })
}

const populateLeisure = (regions, data) => {

  if (!data) {
    return []
  }

  let leisure = mapRegionsWithinDistance(regions, data.sport, 'nsports', 5000, false)
  const breaksSports = getBreaks(leisure, 'nsports')

  leisure = mapRegionsWithinDistance(regions, data.entertainment, 'nentertainment', 2000, false)
  const breaksEntertainment = getBreaks(leisure, 'nentertainment')

  return leisure.filter((e) => e['nsports'] > 0 | e['nentertainment'] > 0).map((row) => {
    row.score_sports = getBreakPoint(row.nsports, breaksSports)
    row.score_entertainment = getBreakPoint(row.nentertainment, breaksEntertainment)
    row.score = Math.floor((row.score_sports + row.score_entertainment) / 2)
    return row
  })
}

const populate = (centroids, category, data) => {
  const mapping = {}
  let populatedData;

  switch (category) {
    case 'housing':
      populatedData = populateHousing(centroids, data)
      break
    case 'work':
      populatedData = populateWork(centroids, data)
      break
    case 'leisure':
      populatedData = populateLeisure(centroids, data)
      break
    default:
      populatedData = []
      break
  }
  populatedData.forEach((row) => {
    mapping[row.postcode] = row
  })
  return mapping

}

export const scores = (state = {}, action) => {
  const data = {}
  switch (action.type) {
    case 'RECEIVE_DATA':
      const regions = state.centroids
      data[action.category] = populate(regions, action.category, action.data)
      return Object.assign({}, state, {
        ...data
      })
    case 'RECEIVE_CENTROIDS':
      data['centroids'] = action.data
      return Object.assign({}, state, {
        ...data
      })
    default:
      return state
  }
}