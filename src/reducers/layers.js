const layer = (state = {}, action) => {
    switch (action.type) {
        case 'RECEIVE_VECTOR_LAYER':
            return {
                type: 'vector',
                url: action.url,
                key: action.layer
            }
        default:
            return state
    }
}


export const layers = (state = [], action) => {
    switch (action.type) {
        case 'RECEIVE_VECTOR_LAYER':
            return [
                ...state,
                layer(undefined, action)
            ]
        default:
            return state
    }
}
