import _ from '../translate'


const preferences = [
  {key: 'public_transport', label: _`Access to public transport`},
  {key: 'crime', label: _`Low crime rate`},
  {key: 'nature', label: _`Close to nature`},
  {key: 'beach', label: _`Close to shops`},
  {key: 'shops', label: _`Close to the beach`},
]

export default preferences