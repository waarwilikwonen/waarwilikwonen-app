import fetch from 'isomorphic-fetch'
import qs from 'qs'


export const receiveData = (category, data) => {
  return {
    type: 'RECEIVE_DATA',
    category,
    data,
    receivedAt: Date.now()
  }
}


export const receiveCentroids = (data) => {
  return {
    type: 'RECEIVE_CENTROIDS',
    data,
    receivedAt: Date.now()
  }
}

export const setParams = (data) => {
  return {
    type: 'SET_PARAMS',
    data,
    receivedAt: Date.now()
  }
}

export const fetchHousing = (data) => {
  return (dispatch, getState) => {
    const token = getState().apikey.apikey
    const data = getState().params

    const params = {
      rooms: 1
      + data.household.children4 || 0
      + data.household.children12 || 0
      + data.household.chilren18 || 0,
      modality: data.housing.modality,
      budget: data.housing.budget,
      type: data.housing.type
    }

    fetch(`${config.api_url}/housing.json?${qs.stringify(params)}&token=${token}`)
      .then(response => response.json())
      .then(json => dispatch(receiveData('housing', json)))
  }
}

export const fetchWork = (data) => {
  return (dispatch, getState) => {
    const token = getState().apikey.apikey
    const data = getState().params

    const keywords = (data.work.keywords || '')
    const partnerKeywords = (data.work.partnerKeywords || '')

    const params = {
      keywords: (keywords + ' ' + partnerKeywords).trim()
    }

    fetch(`${config.api_url}/work.json?${qs.stringify(params)}&token=${token}`)
      .then(response => response.json())
      .then(json => dispatch(receiveData('work', json)))
  }
}

export const fetchLeisure = (data) => {
  return (dispatch, getState) => {
    const token = getState().apikey.apikey
    const data = getState().params

    const sports = data.leisure.sports || []
    const entertainment = data.leisure.entertainment || []
    const paramsSport = {
      keywords: sports.join(' ')
    }
    const paramsEntertainment = {
      keywords: entertainment.join(' ')
    }

    fetch(`${config.api_url}/poi.json?${qs.stringify(paramsSport)}&token=${token}`)
      .then(response => response.json())
      .then(jsonSport => {
        fetch(`${config.api_url}/poi.json?${qs.stringify(paramsEntertainment)}&token=${token}`)
          .then(response => response.json())
          .then(jsonEntertainment => dispatch(receiveData('leisure', {
            sport: jsonSport,
            entertainment: jsonEntertainment
          })))
      })

  }
}

export const fetchData = () => {
  return (dispatch, getState) => {
    dispatch(fetchHousing())
    dispatch(fetchWork())
    dispatch(fetchLeisure())
  }
}

export const fetchRegionCentroids = () => {
  return (dispatch, getState) => {
    const token = getState().apikey.apikey
    fetch(`${config.api_url}/regions.json?centroids=true&token=${token}`)
      .then(response => response.json())
      .then(json => dispatch(receiveCentroids(json)))
  }
}