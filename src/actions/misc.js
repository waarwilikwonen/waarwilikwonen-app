import fetch from 'isomorphic-fetch'


export const dialogChanged = (open) => {
  return {
    type: 'DIALOG_CHANGED',
    open,
    receivedAt: Date.now()
  }
}

export const isLoading = (loading) => {
  return {
    type: 'LOADING',
    loading,
    receivedAt: Date.now()
  }
}


export const receiveApikey = (data) => {
  return {
    type: 'RECEIVE_APIKEY',
    apikey: data.apikey,
    expires: data.expires,
    receivedAt: Date.now()
  }
}



export const openDialog = (open) => {
  return (dispatch, getState) => {
     dispatch(dialogChanged(open))
  }
}


export const validateRecaptcha = (token) => {
  return (dispatch, getState) => {
    fetch(`${config.api_url}/auth`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        publickey: config.publickey,
        recaptcha_response: token
      })
    })
    .then(response => response.json())
    .then(json => dispatch(receiveApikey(json)))
  }
}