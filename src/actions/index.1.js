import queryString from "query-string"
import fetch from 'isomorphic-fetch'


export const receiveLayer = (layer, data) => {
    return {
        type: 'RECEIVE_LAYER',
        layer,
        data,
        receivedAt: Date.now()
    }
}

export const receiveTables = (data) => {
    return {
        type: 'RECEIVE_TABLES',
        receivedAt: Date.now(),
        data
    }
}

export const fetchLayer = (layer, params) => {
    return dispatch => {
        const qs = queryString.stringify(params)
        return fetch(`http://localhost:8081/${layer}.json?${qs}`)
            .then(response => response.json())
            .then(json => dispatch(receiveLayer(layer, json)))
    }
}

export const fetchLayers = () => {
    return (dispatch, getState) => {
        const state = getState()
        Object.keys(state.layerParams).forEach((key) => {
            const params = Object.assign({}, state.layerParams[key]);
            if (params.enabled === true) {
                delete params.enabled
                dispatch(fetchLayer(key, params))
            }
        })
    }
}

export const fetchTables = () => {
    return dispatch => {
        fetch(`http://localhost:8081/tables`)
            .then(response => response.json())
            .then(json => dispatch(receiveTables(json)))
    }
}

export const updateLayerParams = (layer, name, value) => {
    return {
        type: 'UPDATE_LAYER_PARAMS',
        receivedAt: Date.now(),
        layer,
        name,
        value
    }
}

export const enableLayer = (layer) => {
    return {
        'type': 'ENABLE_LAYER',
        receivedAt: Date.now(),
        layer
    }
}

export const openMenu = (open) => {
    return {
        type: 'OPEN_MENU',
        open,
        receivedAt: Date.now()
    }
}