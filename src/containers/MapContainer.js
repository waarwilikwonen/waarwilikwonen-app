import { connect } from 'react-redux'

import Map from '../components/Map'


const mapStateToProps = (state) => {

    const regions = state.scores.centroids
    const scores = {
        housing: state.scores.housing || {},
        work: state.scores.work || {},
        leisure: state.scores.leisure || {}
    }
    const stops = (regions) ? regions.map((region) => {
        const score = ['housing', 'work', 'leisure'].map((k) => {
            if (!scores[k]) {
                return 0
            }
            const z = scores[k][region.postcode]
            return (z) ? z.score : 0
        }).reduce((a, b) => a + b, 0) / 3
        const color = `hsla(0, 100%, 50%, ${score / 10. * 0.8})`
        return [region.postcode, color]
    }) : [[1234, `rgba(0,0,0,1)`]]

    return {
        layers: (state.loading) ? state.layers : [],
        stops: stops,
        options: {
            container: 'map',
            style: 'mapbox://styles/mapbox/outdoors-v9',
            center: [5.5, 52.2],
            zoom: 6.3
        },
        popupContext: {
            layer: 'regions',
            featureProperty: 'postcode',
            properties: scores,
            title: 'Scores:'
        }
        // onClick: (event, map) => {

        //     const postcode = feature.properties.postcode
        //     const currentScores = []
        //     console.log(scores)
        //     Object.keys(scores).forEach((key) => {
        //         currentScores.push({ key, value: scores[key][postcode] })
        //     })

        //     return { title: 'Scores', properties: currentScores }
        // }
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLoad: () => {
            // dispatch(fetchRegionCentroids())
            // dispatch(fetchVectorLayer())
        }
    }
}

const MapContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Map)

export default MapContainer
