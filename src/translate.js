import translations from './i18n'

const lang = 'nl' //navigator.language.split('-')[0]

export default function _(template, ...expressions) {
    if (typeof template === 'string') {
        return translations[lang][template] || template
    } else if (template.length === 1) {
        const orig = template[0]
        let t = translations[lang][orig] || orig
        return t
    } else {
        return template.reduce((accumulator, part, i) => {
            const orig = `${accumulator}$\{${i}\}${part}`
            let t = translations[lang][orig] || orig
            t = t.replace(`$\{${i}\}`, expressions[i - 1])
            return t
        })
    }
}