import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { openMenu, updateLayerParams, enableLayer, fetchLayers, fetchTables } from '../actions'
import { strCapitalize } from '../utils'

import { Drawer, TextField, Toggle, RaisedButton, SelectField, MenuItem } from 'material-ui'


const operatorToLabel = {
  min: 'From',
  max: 'To',
  eq: "?",
  eq_choices: "Select"
}

const styles = {
  field: {
    width: '50%'
  },
  container: {
    margin: '10px'
  },
  fieldDescription: {
    fontSize: '10px'
  },
  selectField: {
    width: '100%',
  }
}

class LayerControls extends Component {
  constructor(props) {
    super(props)
    this.state = {
      select: null
    }
  }

  componentDidMount() {
    const { dispatch } = this.props
    dispatch(fetchTables())
  }

  handleParamChange(table, field, operator, value) {
    const qField = `${field.name}__${operator}`
    this.props.dispatch(updateLayerParams(table, qField, value))
  }

  handleSelectChange(value, table, field, operator) {
    this.setState({select: value})
    this.handleParamChange(table, field, operator, value)
  }

  renderField(table, field, operator) {
    if (field.choices) {
      const key = `${table}_${field.name}_${operator}`
      return (<SelectField style={styles.selectField}
        floatingLabelFixed={true}
        floatingLabelText={field.description}
        onChange={(e, i, value) => this.handleSelectChange(value, table, field, operator)}
        value={this.state.select}
        >
        {field.choices.map((choice) => (
          <MenuItem key={key + '_' + choice} value={choice[0]} primaryText={choice[1]} />
        ))}
      </SelectField>)
    } else {
      return (<TextField
        hintText={operatorToLabel[operator]}
        style={styles.field}
        floatingLabelFixed={true}
        floatingLabelText={strCapitalize(operator) + ' ' + field.description}
        onChange={(e) => this.handleParamChange(table, field, operator, e.target.value)}
        />)
    }
  }

  render() {
    const tables = this.props.tables

    return (
      <Drawer
        open={this.props.menuState.open}
        docked={false}
        onRequestChange={(open) => this.props.dispatch(openMenu(open))}
        width={350}
        >
        {tables.map((table) => (
          <div key={table.name} style={styles.container}>
            <Toggle
              label={table.description}
              onToggle={() => this.props.dispatch(enableLayer(table.name))}
              />
            {table.fields.map((field) => (
              <div key={table.name + '_' + field.name}>
                  {field.operators.map((operator) => (
                    <span key={table.name + '_' + field.name + '_' + operator}>
                      {this.renderField(table.name, field, operator)}
                    </span>
                  ))}
              </div>
            ))}
          </div>
        ))}
        <RaisedButton label="Apply" primary={true} style={styles.container}
          onTouchTap={() => this.props.dispatch(fetchLayers())} />
      </Drawer>
    )
  }
}

LayerControls.propTypes = {
  dispatch: PropTypes.func.isRequired,
  menuState: PropTypes.object,
  tables: PropTypes.array
}

function mapStateToProps(state) {
  const {menu, tables } = state
  return { menuState: menu, tables }
}

export default connect(mapStateToProps)(LayerControls)
