import React, { PropTypes, Component } from 'react'
import ReactDOM from 'react-dom'

import MapboxGL from 'mapbox-gl/dist/mapbox-gl.js'

// import { GeoJSONLayer, VectorLayer } from './layers'
import GeoJSONLayer from './layers/GeoJSONLayer'
import VectorLayer from './layers/VectorLayer'


class Map extends Component {
    constructor() {
        super()
        this.state = {}
    }

    componentDidMount() {
        const { options, onLoad } = this.props

        MapboxGL.accessToken = config.mapboxgl_access_token
        const map = new MapboxGL.Map(options)
        const _onClick = this.onClick.bind(this)
        map.on('click', _onClick)

        this.setState({ map });

        if (onLoad) {
            map.on('load', onLoad)
        }
    }

    onClick(event) {
        const { popupContext } = this.props
        const { map } = this.state
        const { layer, featureProperty, properties, title } = popupContext

        const features = map.queryRenderedFeatures(event.point, { layers: [layer] });
        if (!features.length) {
            return
        }
        const property = features[0].properties[featureProperty]
        const items = Object.keys(properties).map((key) => {
            const value = properties[key][property] ? properties[key][property].score : 0
            return { key, value }
        }).filter((item) => item !== null)

        const id = `map-popup-${Math.floor(Math.random() * 100000)}`

        new MapboxGL.Popup()
            .setLngLat(event.lngLat)
            .setHTML(`<div id="${id}"></div>`)
            .addTo(map);

        ReactDOM.render(
            <div>
                <h6>{title}</h6>
                <table>
                    <tbody>
                        <tr key={featureProperty}>
                            <th>{featureProperty}</th>
                            <td>{property}</td>
                        </tr>
                        {items.map((item) => (
                            <tr key={item.key}>
                                <th>{item.key}</th>
                                <td>{item.value}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>,
            document.getElementById(id)
        )

    }

    getChildContext() {
        if (this.state.map) {
            return {
                map: this.state.map
            }
        }
    }

    render() {
        const containerStyle = {
            width: '100vw',
            height: '100vh',
        }
        const { layers, options, stops } = this.props
        return (
            <div id={options.container} style={containerStyle}>
                {layers.map(layer => {
                    if (layer.type === 'geojson')
                        return (<GeoJSONLayer key={layer.key} data={layer.data} />)
                    else
                        return (<VectorLayer key={layer.key} url={layer.url} stops={stops} name={layer.key} />)
                })}
            </div>
        )
    }
}

Map.propTypes = {
    layers: PropTypes.array.isRequired,
    options: PropTypes.object,
    onLoad: PropTypes.func,
    stops: PropTypes.array,
    onClick: PropTypes.func
}

Map.childContextTypes = {
    map: PropTypes.object.isRequired,
}

export default Map