import React from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { deepOrange500 } from 'material-ui/styles/colors'
import getMuiTheme from 'material-ui/styles/getMuiTheme'

import injectTapEventPlugin from 'react-tap-event-plugin'

import MapContainer from '../containers/MapContainer'
import QuestionDialogContainer from '../containers/QuestionDialogContainer'
import LoadingBoxContainer from '../containers/LoadingBoxContainer'

injectTapEventPlugin();


const styles = {
  container: {
    width: '100vw',
    height: '100vh'
  },
};

const muiTheme = getMuiTheme({
  palette: {
    accent1Color: deepOrange500,
  },
});

const App = () => (
  <MuiThemeProvider muiTheme={muiTheme}>
    <div style={styles.container}>
      <QuestionDialogContainer/>
      <MapContainer/>
      <LoadingBoxContainer/>
    </div>
  </MuiThemeProvider>
)

export default App