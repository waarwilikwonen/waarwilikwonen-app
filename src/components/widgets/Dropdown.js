import React, { PropTypes } from 'react'
import { findDOMNode } from 'react-dom'
import Dialog from './Dialog'

class Dropdown extends React.Component {

  componentDidMount() {
    const { onClose } = this.props
    this._onClose = onClose
    this._onKeyDown = this.onKeyDown.bind(this)

    this.uuidDialog = `dialog${Math.floor(Math.random() * 1000000)}`
    this.buttonIdx = -1

    document.getElementById('root').addEventListener('click', this._onClose)
    document.getElementById('root').addEventListener('keydown', this._onKeyDown)
  }

  componentWillUnmount () {
    document.getElementById('root').removeEventListener('click', this._onClose)
    document.getElementById('root').removeEventListener('keydown', this._onKeyDown)
  }

  componentDidUpdate() {
    const offset = this.props.offset || 0
    const dialog = findDOMNode(this.refs[this.uuidDialog])
    if (this.props.open) {
      const elem = findDOMNode(this.props.anchor)
      const rect = elem.getBoundingClientRect()
      // const bodyRect = document.body.getBoundingClientRect()
      // const dialogRect = document.getElementsByTagName('dialog')[0]
      dialog.style.position = 'absolute'
      const elemY = (this.refs[this.uuidDialog].state.isPolyfill) ?
        elem.offsetTop
        : elem.offsetTop
      dialog.style.top = `${elemY + offset}px`
      dialog.style.width = `${rect.width}px`
      dialog.classList.add('select-dialog-open')
    } else {
      dialog.classList.remove('select-dialog-open')
    }

  }

  onKeyDown(event) {
    const { open, onClose } = this.props

    if (event.keyCode === 40 && open) {
      this.buttonIdx ++;
      let elem = this.refs[`btn${this.buttonIdx}`]
      if (elem === null)  {
        this.buttonIdx = 0
        elem = this.refs[`btn${this.buttonIdx}`]
      }
      findDOMNode(elem).focus()
    } else if (event.keyCode === 38 && open && this.buttonIdx > 0) {
      this.buttonIdx --;
      let elem = this.refs[`btn${this.buttonIdx}`]
      findDOMNode(elem).focus()
    } else if (event.keyCode === 27 && open) {
      onClose()
    }
  }

  render() {
    const { children, onActivate, selected, open, modal } = this.props
    return (
      <Dialog
        className="mdlp-dropdown"
        ref={this.uuidDialog}
        open={open && children.length > 0}
      >
        {React.Children.map(children, (child, index) => {
          return React.cloneElement(child, {
            key: index,
            ref: `btn${index}`,
            selected: child.props.value === selected,
            onClick: (e) => onActivate(child)
          })
        })}
      </Dialog>
    )
  }
}

Dropdown.propTypes = {
    onActivate: PropTypes.func,
    selected: PropTypes.any,
    open: PropTypes.bool,
    anchor: PropTypes.object.isRequired,
    offset: PropTypes.number,
    focus: PropTypes.number
}

export default Dropdown