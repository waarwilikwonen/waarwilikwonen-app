import React, { PropTypes } from 'react'
import { findDOMNode } from 'react-dom'

import { Textfield, Chip } from 'react-mdl'
import Dropdown from './Dropdown'
import Option from './Option'


class Autocomplete extends React.Component {
  constructor() {
    super()
    this.state = {
      chips: [],
      label: '',
      value: '',
      renderDropdown: false
    }
    this.uuidTextfield = `textfield${Math.floor(Math.random() * 1000000)}`
    this.uuidDropdown = `dropdown${Math.floor(Math.random() * 1000000)}`
  }

  componentWillMount() {
    const { value, multiple } = this.props
    if (value) {
      if (multiple) {
        this.setState({ chips: value })
      } else {
        this.setState({ label: value.label, value: value.value })
      }
    }
  }

  componentDidUpdate() {
    findDOMNode(this.refs[this.uuidTextfield].inputRef).focus()
  }

  searchEntries() {
    const { datasource } = this.props
    const { chips, label } = this.state

    const limit = this.props.limit || 5

    const expr = new RegExp(label.trim(), 'i')

    const addedValues = chips.map((chip) => chip.value)

    const filtered = datasource.filter((entry) => {
      if (addedValues.indexOf(entry.key) > -1) {
        return false
      } else if (typeof entry === 'object') {
        return entry.label.match(expr) !== null
      } else if (typeof entry === 'string') {
        return entry.match(expr) !== null
      }
      return false
    })
    return filtered.slice(0, limit)
  }

  maybeShowDropdown(searchValue) {
    const minLength = (this.props.minLength === undefined) ? 3 : this.props.minLength

    if (searchValue.length >= minLength)
      this.setState({ renderDropdown: true, label: searchValue })
    else
      this.setState({ renderDropdown: false, label: searchValue })
  }

  render() {
    const { renderDropdown, label, chips } = this.state
    const { multiple, label: textfieldLabel, floatingLabel, onChange } = this.props
    return (
      <div className="mdlp-autocomplete">
        {(multiple) &&
          chips.map((chip) => {
            return (
              <Chip
                key={chip.value}
                onClose={(e) => {
                  chips.splice(chips.indexOf(chip), 1)
                  this.setState({ chips })
                }
              }>
                {chip.label}
              </Chip>
            )
          })
        }

        <Dropdown
          ref={this.uuidDropdown}
          onActivate={(child) => {
            const _label = child.props.children
            const _value = child.props.value

            if (multiple) {
              const chip = {label: _label, value: _value}
              if (chips.indexOf(chip) === -1) {
                chips.push(chip)
                this.setState({
                  value: '', label: '',
                  chips: chips,
                  renderDropdown: false
                })
                onChange(chips)
              }
            } else {
              this.setState({
                value: _value,
                label: _label,
                renderDropdown: false
              })
              onChange({value: _value, label: _label})
            }
          }}
          open={renderDropdown || false}
          anchor={this.refs[this.uuidTextfield]}
          offset={50}
          modal={false}
          onClose={() => this.setState({ renderDropdown: false })}
          >
          {this.searchEntries().map((entry) => {
            const _label = (typeof entry === 'string') ? entry : entry.label
            const _value = (typeof entry === 'string') ? entry : entry.key
            return (
              <Option value={_value} key={_value}>{_label}</Option>
            )
          })}
        </Dropdown>
        <Textfield
          ref={this.uuidTextfield}
          label={textfieldLabel}
          floatingLabel={floatingLabel}
          onChange={(event) => {
            this.maybeShowDropdown(event.target.value)
          }}
          onClick={(event) => {
            this.maybeShowDropdown(event.target.value)
          }}
          value={label || ''}
          />
      </div>
    )
  }
}

Autocomplete.propTypes = {
  label: PropTypes.string.isRequired,
  floatingLabel: PropTypes.bool,
  multiple: PropTypes.bool,
  datasource: PropTypes.array.isRequired,
  minLength: PropTypes.number,
  limit: PropTypes.number,
  onChange: PropTypes.func
}

export default Autocomplete