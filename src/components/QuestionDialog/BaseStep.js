import React, { PropTypes } from 'react'
import ReactDOM from 'react-dom'

import Actions from './Actions'
import _ from '../../translate'


class BaseStep extends React.Component {
  componentWillMount() {
    this.state = this.props.values || {

    }
  }

  componentDidUpdate() {
    const { onNextStep } = this.props

    if (this.state._validating) {
      const form = ReactDOM.findDOMNode(this)
      delete this.state._validating

      if(form.checkValidity()) {
        onNextStep(this.state)
      }
    }
  }

  render() {
    const { onNextStep, onPreviousStep, onFinished, onClose } = this.props
    return (
      <form action='#'>
        {this.renderStep()}
        <Actions
          onNextStep={(onNextStep) ? () => {
            this.setState({ _validating: true})
          } : undefined}
          stepIndex={0}
          onPreviousStep={onPreviousStep}
          onFinished={onFinished}
          onClose={onClose}/>

      </form>
    )
  }
}

BaseStep.propTypes = {
    onNextStep: PropTypes.func,
    onPreviousStep: PropTypes.func,
    onFinished: PropTypes.func,
    onClose: PropTypes.func,
    values: PropTypes.object
}


export default BaseStep
