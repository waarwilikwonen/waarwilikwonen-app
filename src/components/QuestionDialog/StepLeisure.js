import React from 'react'

import Autocomplete from '../widgets/Autocomplete'


import BaseStep from './BaseStep'

import _ from '../../translate'
import dataSourceSports from '../../datasources/sports'
import datasourcesLeisure from '../../datasources/leisure'


class StepLeisure extends BaseStep {
  renderStep() {
    const { entertainment, sports } = this.state
    return (
      <div>
        What entertainment you enjoy?
        <Autocomplete
          label={_`Click to see more options`}
          multiple={true}
          datasource={datasourcesLeisure}
          minLength={0}
          onChange={(entertainment) => {
            this.setState({ entertainment })
          }}
          value={entertainment}
        />
        What sports do you or your family practice?
        <Autocomplete
          label={_`i.e. soccer, basketball, etc.`}
          multiple={true}
          datasource={dataSourceSports}
          onChange={(sports) => {
            this.setState({ sports })
          }}
          value={sports}
        />
      </div>
    )
  }
}

export default StepLeisure