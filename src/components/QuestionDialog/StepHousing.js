import React from 'react'

import { Textfield } from 'react-mdl'
import Select from '../widgets/Select'
import Option from '../widgets/Option'

import BaseStep from './BaseStep'

import _ from '../../translate'


class StepHousing extends BaseStep {

  renderStep() {
    return (
      <div>
        <Select
          label={_`Do you want to buy or rent?`}
          floatingLabel
          onChange={(value) => this.setState({ modality: value})}
          value={this.state.modality}
          required={this.state._validating}
        >
          <Option value="price">{_`Buy`}</Option>
          <Option value="rent">{_`Rent`}</Option>
        </Select>
        <Textfield
          label={_`What is your budget?`}
          floatingLabel
          pattern="[0-9]"
          type="number"
          onChange={(event) => this.setState({ budget: event.target.value })}
          value={this.state.budget}
          required={this.state._validating}
        />
        <Select
          label={_`Type of housing?`}
          floatingLabel
          onChange={(value) => this.setState({ type: value })}
          value={this.state.type}
          required={this.state._validating}
        >
          <Option value={0}>{_`House`}</Option>
          <Option value={1}>{_`Appartment`}</Option>
          <Option value={2}>{_`Room`}</Option>
        </Select>
      </div>
    )
  }
}

export default StepHousing