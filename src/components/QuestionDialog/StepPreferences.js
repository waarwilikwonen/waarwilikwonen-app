import React from 'react'

import { List, ListItem, ListItemAction, ListItemContent, Checkbox } from 'react-mdl'

import BaseStep from './BaseStep'

import _ from '../../translate'

import datasourcesPreferences from '../../datasources/preferences'


class StepLeisure extends BaseStep {
  renderStep() {
    let { selected } = this.state
    selected = selected || []
    return (
      <div>
        Select up to 10 items which are extra important
        <List>
          {datasourcesPreferences.map((e) => (
              <ListItem key={e.key}>
                <ListItemContent>{e.label}</ListItemContent>
                <ListItemAction>
                  <Checkbox
                    onChange={(evt) => {
                      if (evt.target.checked)
                        selected.push(e.key)
                      else
                        selected.splice(selected.indexOf(e.key), 1)
                      this.setState({ selected }) }}
                    checked={selected.indexOf(e.key) > -1}
                    ripple
                  />
                </ListItemAction>
              </ListItem>
            ))}
        </List>
      </div>
    )
  }
}

export default StepLeisure