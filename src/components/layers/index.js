import GeoJSONLayer from './GeoJSONLayer'
import VectorLayer from './VectorLayer'

export default { GeoJSONLayer, VectorLayer }