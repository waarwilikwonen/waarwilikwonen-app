import { Component, PropTypes } from 'react'


class VectorLayer extends Component {
    render() {
        const map = this.context.map
        const { url, name, stops } = this.props
        const sourceOptions = {
            type: "vector",
            name,
            tiles: [url]
        }
        const options = {
            id: name,
            type: "fill",
            source: name,
            'source-layer': name,
            paint: {
                'fill-color': {
                    'property': 'postcode',
                    'type': 'categorical',
                    'stops': stops
                }
                // 'fill-color': 'red',
                // 'fill-opacity': 0.2,
                // 'fill-outline-color': 'black'
            }
        }

        try {
            map.removeSource(name)
            map.removeLayer(name)
        } catch(err) { }

        map.addSource(name, sourceOptions)
        map.addLayer(options)
        return null
    }
}

VectorLayer.propTypes = {
    map: PropTypes.object,
    url: PropTypes.string,
    stops: PropTypes.array
}

VectorLayer.contextTypes = {
    map: PropTypes.object.isRequired,
}

export default VectorLayer