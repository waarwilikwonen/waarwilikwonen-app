
export const strCapitalize = (s) =>
    s.replace(/\b\w/g, l => l.toUpperCase())
